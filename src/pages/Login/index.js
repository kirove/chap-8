import React from "react";
import "./login.scss";
import { Button, Gap, InputBySass, TitleBox } from "../../components";
import { UsernameIcon, PasswordIcon } from "../../assets";
import { Link } from "react-router-dom";

const Login = () => {
  return (
    <div className="bg-part">
      <div className="log-reg-box">
        <div className="input-box-log-reg">
          <TitleBox titleBoxProp="Login" />
          <Gap height={30} />
          <InputBySass iconProp={UsernameIcon} labelProp="Username" />
          <Gap height={30} />
          <InputBySass
            type="password"
            iconProp={PasswordIcon}
            labelProp="Password"
          />
        </div>
        <Gap height={45} />
        <Link to={"/dashboard"}>
          <Button buttonName="Login" />
        </Link>
      </div>
    </div>
  );
};

export default Login;
