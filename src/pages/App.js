import "./App.css";
import { Routes, Route } from "react-router-dom";
import {
  CreateRoom,
  GameHistory,
  Login,
  Dashboard,
  Registration,
  VersusComputer,
  VersusPlayer,
  FourZeroFour,
  MainPage,
} from "../pages";

function App() {
  return (
    <Routes>
      <Route path="/" element={<MainPage />} />
      <Route path="/creroom" element={<CreateRoom />} />
      <Route path="/gamehist" element={<GameHistory />} />
      <Route path="/login" element={<Login />} />
      <Route path="/dashboard" element={<Dashboard />} />
      <Route path="/regist" element={<Registration />} />
      <Route path="/vercom" element={<VersusComputer />} />
      <Route path="/verplay" element={<VersusPlayer />} />
      <Route path="*" element={<FourZeroFour />} />
    </Routes>
  );
}

export default App;
