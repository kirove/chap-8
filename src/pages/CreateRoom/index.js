import React from "react";
import "./createRoom.scss";
import { Gap, ButtonGreen } from "../../components";
import { BoxRoomName, PaperPic, RockPic, ScissorPic } from "../../assets";
import { Link } from "react-router-dom";

const CreateRoom = () => {
  return (
    <div className="bg-cre-room">
      <p className="text-remark-cre-room">
        Please input the room's name you want to create
      </p>
      <Gap height={15} />
      <div className="input-cre-room">
        <img className="input-cre-room-img" src={BoxRoomName} alt=""></img>
        <input className="input-cre-room-label"></input>
      </div>
      <Gap height={150} />
      <p className="text-remark-cre-room">Please pick your choice</p>
      <Gap height={60} />
      <div className="choice-box">
        <img className="rock-choice" src={RockPic} alt=""></img>
        <Gap width={100} />
        <img className="scissor-choice" src={ScissorPic} alt=""></img>
        <Gap width={100} />
        <img className="paper-choice" src={PaperPic} alt=""></img>
        <Gap height={100} />
      </div>
      <Gap height={150} />
      <Link to={"/dashboard"}>
        <ButtonGreen labelBtnGrn="Save" />
      </Link>
    </div>
  );
};

export default CreateRoom;
