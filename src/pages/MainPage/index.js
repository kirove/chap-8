import React from "react";
import "./mainPage.scss";
import { SmallLogo } from "../../assets";
import { Link } from "react-router-dom";

const MainPage = () => {
  return (
    <div className="bg-main-page">
      <Link to={"/login"}>
        <button className="btn-main-page">
          <img className="img-main-page" src={SmallLogo} alt=""></img>
        </button>
      </Link>
      <p className="txt-main-page">WELCOME</p>
    </div>
  );
};

export default MainPage;
