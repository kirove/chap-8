import React from "react";
import "./versusPlayer.scss";
import { Title, Gap, TitleBoxGame } from "../../components";
import {
  DrawNotif,
  LoseNotif,
  PaperPic,
  RockPic,
  ScissorPic,
  VersusAlert,
  WinNotif,
} from "../../assets";

const VersusPlayer = () => {
  return (
    <div className="bg-ver-ply">
      <div className="style-title-box-game">
        <TitleBoxGame labelBoxTitle="Room A" />
      </div>
      <div className="box-playing">
        <div className="left-ver-ply">
          <Title titleName="Player 1" />
          <Gap height={70} />
          <img className="rock-player-ver-ply" src={RockPic} alt=""></img>
          <Gap height={30} />
          <img className="scissor-player-ver-ply" src={ScissorPic} alt=""></img>
          <Gap height={30} />
          <img className="paper-player-ver-ply" src={PaperPic} alt=""></img>
        </div>
        <div className="remark-middle-ver-ply">
          <img className="remark-vs-ver-ply" src={VersusAlert} alt=""></img>
          <img className="remark-win-ver-ply" src={WinNotif} alt=""></img>
          <img className="remark-lose-ver-ply" src={LoseNotif} alt=""></img>
          <img className="remark-draw-ver-ply" src={DrawNotif} alt=""></img>
        </div>
        <div className="right-ver-ply">
          <Title titleName="???" />
          <Gap height={70} />
          <img className="rock-player-ver-ply" src={RockPic} alt=""></img>
          <Gap height={30} />
          <img className="scissor-player-ver-ply" src={ScissorPic} alt=""></img>
          <Gap height={30} />
          <img className="paper-player-ver-ply" src={PaperPic} alt=""></img>
        </div>
      </div>
    </div>
  );
};

export default VersusPlayer;
