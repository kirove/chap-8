import React from "react";
import "./versusComputer.scss";
import { Title, Gap, ButtonGreen } from "../../components";
import {
  DrawNotif,
  LoseNotif,
  PaperPic,
  RockPic,
  ScissorPic,
  VersusAlert,
  WinNotif,
} from "../../assets";

const VersusComputer = () => {
  return (
    <div className="bg-ver-com">
      <div className="playing-part-ver-com">
        <div className="left-ver-com">
          <Title titleName="Player 1" />
          <Gap height={100} />
          <img className="rock-player-ver-com" src={RockPic} alt=""></img>
          <Gap height={50} />
          <img className="scissor-player-ver-com" src={ScissorPic} alt=""></img>
          <Gap height={50} />
          <img className="paper-player-ver-com" src={PaperPic} alt=""></img>
        </div>
        <div className="remark-middle-ver-com">
          <img className="remark-vs-ver-com" src={VersusAlert} alt=""></img>
          <img className="remark-win-ver-com" src={WinNotif} alt=""></img>
          <img className="remark-lose-ver-com" src={LoseNotif} alt=""></img>
          <img className="remark-draw-ver-com" src={DrawNotif} alt=""></img>
        </div>
        <div className="right-ver-com">
          <Title titleName="Computer" />
          <Gap height={100} />
          <img className="rock-com-ver-com" src={RockPic} alt=""></img>
          <Gap height={50} />
          <img className="scissor-com-ver-com" src={ScissorPic} alt=""></img>
          <Gap height={50} />
          <img className="paper-com-ver-com" src={PaperPic} alt=""></img>
        </div>
      </div>
      <Gap height={20} />
      <div className="btn-play-again">
        <ButtonGreen labelBtnGrn="Play" />
      </div>
    </div>
  );
};

export default VersusComputer;
