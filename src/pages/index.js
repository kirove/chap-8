import CreateRoom from "./CreateRoom";
import GameHistory from "./GameHistory";
import Login from "./Login";
import Dashboard from "./Dashboard";
import Registration from "./Registration";
import VersusComputer from "./VersusComputer";
import VersusPlayer from "./VersusPlayer";
import FourZeroFour from "./404";
import MainPage from "./MainPage";

export {
  CreateRoom,
  GameHistory,
  Login,
  Dashboard,
  Registration,
  VersusComputer,
  VersusPlayer,
  FourZeroFour,
  MainPage,
};
