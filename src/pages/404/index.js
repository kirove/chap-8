import React from "react";
import "./404.scss";
import { ButtonYellow } from "../../components";
import { Link } from "react-router-dom";

const FourZeroFour = () => {
  return (
    <div className="bg-404">
      <Link to={"/"}>
        <ButtonYellow labelBtnYlw="Go Back" />
      </Link>
    </div>
  );
};

export default FourZeroFour;
