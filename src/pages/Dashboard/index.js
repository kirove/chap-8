import React, { useState } from "react";
import "./dashboard.scss";
import { ButtonGreen, ButtonYellow, Gap } from "../../components";
import { Avatar, ProfileBoard, RectangleHorz, SmallLogo } from "../../assets";
import InfiniteScroll from "react-infinite-scroll-component";
import { Link } from "react-router-dom";

const Dashboard = () => {
  const [dataHistory] = useState(Array.from({ length: 5 }));
  const [Rooms] = useState([
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
    {
      roomName: "A",
      winner: "Pingkan",
    },
  ]);
  const [Bio] = useState([
    {
      fullName: "Cha Eun Woo",
      phoneNumber: "08989999",
      address: "Makassar",
      dateofBirth: "March 30th",
    },
  ]);

  return (
    <div className="bg-dash">
      <div className="main-part-dash">
        <div className="left-part-dash">
          <img className="small-pic" src={SmallLogo} alt="" />
          <Gap height={30} />
          <p className="notes-dashboard">
            Tired of waiting? Let's play with Computer, so you will never <br />
            miss the fun.
          </p>
          <Gap height={30} />
          <Link to={"/vercom"}>
            <ButtonGreen labelBtnGrn="Play" />
          </Link>
        </div>
        <div className="middle-part-dash">
          <div className="middle-styling-position">
            <p className="notes-dashboard">Create a room or pick a room.</p>
            <img className="box-outer-middle" src={RectangleHorz} alt=""></img>
            <div className="btn-middle">
              <Link to={"/creroom"}>
                <ButtonYellow labelBtnYlw="Create Room" />
              </Link>
            </div>
            {/* <button> */}
            <div className="list-room-dash">
              <InfiniteScroll
                dataLength={dataHistory.length}
                endMessage={
                  <p
                    style={{
                      fontFamily: "Chicle",
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    You've reach the end
                  </p>
                }
                height={350}
              >
                {Rooms.map((room) => {
                  return (
                    <div className="list-dash-box">
                      <div className="container text-center style-container-dash">
                        <div className="row align-items-start style-row-dash">
                          <button className="btn-room-dash">
                            <div className="col style-col-top">
                              Room {room.roomName}
                            </div>
                            <div className="col style-col-btm">
                              winner: {room.winner}
                            </div>
                          </button>
                          <button className="btn-room-dash">
                            <div className="col style-col-top">
                              Room {room.roomName}
                            </div>
                            <div className="col style-col-btm">
                              winner: {room.winner}
                            </div>
                          </button>
                          <button className="btn-room-dash">
                            <div className="col style-col-top">
                              Room {room.roomName}
                            </div>
                            <div className="col style-col-btm">
                              winner: {room.winner}
                            </div>
                          </button>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </InfiniteScroll>
            </div>
          </div>
        </div>
        <div className="right-part-dash">
          <div className="right-styling-position">
            <img className="box-profile" src={ProfileBoard} alt="" />
            <p className="notes-profile">Profile</p>
            <div className="btn-hist">
              <Link to={"/gamehist"}>
                <ButtonYellow labelBtnYlw="Game History" />
              </Link>
            </div>
            <img className="box-avatar-profile" src={Avatar} alt="" />
            <div className="txt-profile">
              {Bio.map((bio) => {
                return (
                  <div className="txt-profile-style">
                    <table>
                      <tr>
                        <td className="txt-profile-style-tag">Fullname</td>
                        <td className="txt-profile-style-mark">:</td>
                        <td>{bio.fullName}</td>
                      </tr>
                      <tr>
                        <td className="txt-profile-style-tag">Phone Number</td>
                        <td className="txt-profile-style-mark">:</td>
                        <td>{bio.phoneNumber}</td>
                      </tr>
                      <tr>
                        <td className="txt-profile-style-tag">Address</td>
                        <td className="txt-profile-style-mark">:</td>
                        <td>{bio.address}</td>
                      </tr>
                      <tr>
                        <td className="txt-profile-style-tag">Date of Birth</td>
                        <td className="txt-profile-style-mark">:</td>
                        <td>{bio.dateofBirth}</td>
                      </tr>
                    </table>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
