import React from "react";
import "./registration.scss";
import { Button, Gap, InputBySass, TitleBox } from "../../components";
import { UsernameIcon, EmailIcon, PasswordIcon } from "../../assets";
import { Link } from "react-router-dom";

const Registration = () => {
  return (
    <div className="bg-part">
      <div className="log-reg-box">
        <div className="input-box-log-reg">
          <TitleBox titleBoxProp="Registration" />
          <Gap height={30} />
          <InputBySass iconProp={UsernameIcon} labelProp="Username" />
          <Gap height={30} />
          <InputBySass iconProp={EmailIcon} labelProp="Email" />
          <Gap height={30} />
          <InputBySass
            type="password"
            iconProp={PasswordIcon}
            labelProp="Password"
            remarkProp="Min 8 chars"
          />
        </div>

        <Gap height={55} />
        <Link to={"/login"}>
          <Button buttonName="Register" />
        </Link>
      </div>
    </div>
  );
};

export default Registration;
