import React, { useState } from "react";
import "./gameHistory.scss";
import { TitleBoxGame } from "../../components";
import { RectangleHorz } from "../../assets";
import InfiniteScroll from "react-infinite-scroll-component";

const GameHistory = () => {
  const [dataHistory] = useState(Array.from({ length: 1 }));
  const [Histories] = useState([
    {
      roomName: "Room A",
      date: "June 6th 08.00",
      stat: "WIN",
    },
    {
      roomName: "Room A",
      date: "June 6th 08.00",
      stat: "WIN",
    },
    {
      roomName: "Room A",
      date: "June 6th 08.00",
      stat: "WIN",
    },
    {
      roomName: "Room A",
      date: "June 6th 08.00",
      stat: "WIN",
    },
    {
      roomName: "Room A",
      date: "June 6th 08.00",
      stat: "WIN",
    },
    {
      roomName: "Room A",
      date: "June 6th 08.00",
      stat: "WIN",
    },
    {
      roomName: "Room A",
      date: "June 6th 08.00",
      stat: "WIN",
    },
    {
      roomName: "Room A",
      date: "June 6th 08.00",
      stat: "WIN",
    },
    {
      roomName: "Room A",
      date: "June 6th 08.00",
      stat: "WIN",
    },
    {
      roomName: "Room A",
      date: "June 6th 08.00",
      stat: "WIN",
    },
  ]);
  return (
    <div className="bg-gm-hist">
      <div className="tbg-game-hist">
        <TitleBoxGame labelBoxTitle="Game History" />
      </div>
      <div className="box-game-hist">
        <img className="bg-box-game-hist" src={RectangleHorz} alt=""></img>
      </div>
      <div className="list-game-hist">
        <InfiniteScroll
          dataLength={dataHistory.length}
          endMessage={
            <p
              className="bottom-txt-list"
              style={{
                fontFamily: "Chicle",
                display: "flex",
                justifyContent: "center",
              }}
            >
              You've reach the end
            </p>
          }
          height={400}
        >
          {Histories.map((history) => {
            return (
              <div className="list-hist-box">
                <div className="list-hist-box-left">{history.roomName}</div>
                <div className="list-hist-box-middle">{history.date}</div>
                <div className="list-hist-box-right">{history.stat}</div>
              </div>
            );
          })}
        </InfiniteScroll>
      </div>
    </div>
  );
};

export default GameHistory;
