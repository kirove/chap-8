import EmailIcon from "./icon/email-icon.png";
import UsernameIcon from "./icon/username-icon.png";
import PasswordIcon from "./icon/password-icon.png";
import ScissorPic from "./image/gunting.png";
import PaperPic from "./image/kertas.png";
import RockPic from "./image/batu.png";
import YellowBtnTop from "./image/home-btn-top.png";
import YellowBtnSha from "./image/home-btn-shadow.png";
import GreenBtnTop from "./image/play-btn-top.png";
import GreenBtnSha from "./image/play-btn-shadow.png";
import BoxRoomName from "./image/box-room-name.png";
import VersusAlert from "./image/vs-alert.png";
import WinNotif from "./image/win-notif.png";
import LoseNotif from "./image/lose-notif.png";
import DrawNotif from "./image/draw-notif.png";
import BigBox from "./image/bg-box.png";
import RectangleHorz from "./image/bg-box-profile -hori.png";
import RectangleVert from "./image/bg-box-profile-vert.png";
import SmallLogo from "./image/small-logo.png";
import ProfileBoard from "./image/profile-board.png";
import Avatar from "./image/avatar.png";

export {
  EmailIcon,
  UsernameIcon,
  PasswordIcon,
  ScissorPic,
  PaperPic,
  RockPic,
  YellowBtnTop,
  YellowBtnSha,
  GreenBtnTop,
  GreenBtnSha,
  BoxRoomName,
  VersusAlert,
  WinNotif,
  LoseNotif,
  DrawNotif,
  BigBox,
  RectangleHorz,
  RectangleVert,
  SmallLogo,
  ProfileBoard,
  Avatar,
};
