import React from "react";
import "./button.scss";

const Button = ({ buttonName, ...rest }) => {
  return (
    <button
      type="button"
      className="btn btn-primary style-btn style-button"
      {...rest}
    >
      {buttonName}
    </button>
  );
};

export default Button;
