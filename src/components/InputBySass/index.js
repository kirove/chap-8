import React from "react";
import "./inputBySass.scss";
// import { EmailIcon } from "../../assets";

const InputBySass = ({ iconProp, labelProp, remarkProp, ...rest }) => {
  return (
    // <div className="wrapper">
    <div className="form-box form-input">
      <div className="input-box">
        <span>
          <img className="icon-input" src={iconProp} alt=""></img>
        </span>
        <input type="text" className="filling-input" required {...rest}></input>
        <label className="label-input">{labelProp} </label>
        <div className="label-remark">{remarkProp}</div>
      </div>
    </div>
    // </div>
  );
};

export default InputBySass;
