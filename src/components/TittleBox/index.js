import React from "react";
import "./titleBox.scss";

const TitleBox = ({ titleBoxProp }) => {
  return <div className="titleBox">{titleBoxProp}</div>;
};

export default TitleBox;
