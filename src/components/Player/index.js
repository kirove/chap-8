import React from "react";
import "./player.scss";

const Player = ({ playerName }) => {
  return (
    <div>
      <p className="players">{playerName}</p>
    </div>
  );
};

export default Player;
