import Button from "./Button";
import Input from "./Input";
import Player from "./Player";
import Title from "./Tittle";
import Gap from "./Gap";
import InputBySass from "./InputBySass";
import TitleBox from "./TittleBox";
import ButtonGreen from "./ButtonGreen";
import ButtonYellow from "./ButtonYellow";
import TitleBoxGame from "./TitleBoxGame";

export {
  Button,
  Input,
  Player,
  Title,
  Gap,
  InputBySass,
  TitleBox,
  ButtonGreen,
  ButtonYellow,
  TitleBoxGame,
};
