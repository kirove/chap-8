import React from "react";
import "./title.scss";

const Title = ({ titleName }) => {
  return (
    <div>
      <p className="titles">{titleName}</p>
    </div>
  );
};

export default Title;
