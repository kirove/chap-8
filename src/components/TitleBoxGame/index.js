import React from "react";
import "./titleBoxGame.scss";
import { BoxRoomName } from "../../assets";

const TitleBoxGame = ({ labelBoxTitle }) => {
  return (
    <div className="box-room-name">
      <img className="box-room-name-img" src={BoxRoomName} alt=""></img>
      <p className="room-name-label">{labelBoxTitle}</p>
    </div>
  );
};

export default TitleBoxGame;
