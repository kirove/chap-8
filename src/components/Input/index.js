import React from "react";
import "./input.scss";

const Input = ({ labelName, placeHolderName, notes, ...rest }) => {
  return (
    <div class="mb-3 style-input">
      <label for="exampleInputEmail1" class="form-label">
        {labelName}
      </label>
      <input
        type="text"
        class="form-control style-form-control"
        placeholder={placeHolderName}
        {...rest}
      ></input>
      <div class="form-text">{notes}</div>
    </div>
  );
};

export default Input;
