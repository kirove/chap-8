import React from "react";
import "./buttonGreen.scss";
import { GreenBtnSha, GreenBtnTop } from "../../assets";

const ButtonGreen = ({ labelBtnGrn }) => {
  return (
    <div>
      <button className="btn-grn-btn">
        <img className="grn-btn-sha" src={GreenBtnSha} alt=""></img>
        <img className="grn-btn-top" src={GreenBtnTop} alt=""></img>
        <p className="label-grn-btn">{labelBtnGrn}</p>
      </button>
    </div>
  );
};

export default ButtonGreen;
