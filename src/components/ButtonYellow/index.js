import React from "react";
import "./buttonYellow.scss";
import { YellowBtnSha, YellowBtnTop } from "../../assets";

const ButtonYellow = ({ labelBtnYlw }) => {
  return (
    <div>
      <button className="btn-ylw-btn">
        <img className="ylw-btn-sha" src={YellowBtnSha} alt=""></img>
        <img className="ylw-btn-top" src={YellowBtnTop} alt=""></img>
        <p className="label-ylw-btn">{labelBtnYlw}</p>
      </button>
    </div>
  );
};

export default ButtonYellow;
